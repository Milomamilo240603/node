import { Router} from "express";
import { getSuperHeroes,saveSuperHeroes,updateSuperHero,deleteSuperHeroe} from "../Controllers/SuperHeroes.js";
import { subirImagen } from "../Middleware/Storage.js"
import { verificar } from "../Middleware/Auth.js";

const rutas = Router();
rutas.get('/api/marvel',verificar,getSuperHeroes);
rutas.get('/api/marvel/:id',verificar,getSuperHeroes);
rutas.post('/api/marvel',verificar,subirImagen.single('imagen'),saveSuperHeroes);
rutas.put('/api/marvel/:id',verificar,subirImagen.single('imagen'),updateSuperHero);
rutas.delete('/api/marvel/:id',verificar,deleteSuperHeroe);

//rutas.get('/api/no',(req,res)=>{res.send('No jala')});

export default rutas;